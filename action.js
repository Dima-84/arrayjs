"use strict";
//---------------------------------------- Массив
// const myArr = [2, 5, 7, 11, 25];
// console.log(myArr);
// console.log('Длина массива', myArr.length);

// for (let i = 0; i < myArr.length; i++) {
//     console.log(myArr[i]);
// }

//----------------------------------------- Создание массива  

// const newArr = [];

// for (let i = 0; i < 10; i++) {
//     newArr[i] = i;
// }

// newArr[15] = 45;
// console.log(newArr);

//---------------------------------------- Массив с помощью forEach()

// const myArr = [2, 5, 7, 11, 25];

// function myFor(item, index, array) {
//     console.log(item, index, array);
// }

// // myFor(25);
// // myFor('Hello');

// myArr.forEach(myFor);

//----------------------------------------- 0 и четные элементы массива

// const myArr = [2, 5, 7, 11, 25];


// function myFor(item, index) {
//  if (index % 2 === 0) {
//      console.log(item);
//  }
// }
// console.log(myArr);
// myArr.forEach(myFor);

//---------------------------------------- Добавление в конец массива 

// push() - добавление в конец массива
// unshift() - добавление в начало массива

// let myArr = [2, 5, 7, 11, 25];

// myArr[5] = 33;
// console.log(myArr);

// myArr[myArr.length] = 45;
// console.log(myArr);

// myArr[myArr.length] = 108;
// console.log(myArr);

// myArr.push(250);
// console.log(myArr);

// myArr.unshift(1);
// console.log(myArr);

// function addItem(item, arr) {
//     const tempArr = [item];
//     for (let i = 0; i < arr.length; i++) {
//         tempArr.push(arr[i]);
//     }
//     return tempArr;
// }

// myArr = addItem(17, myArr);
// console.log(myArr);

// написать функцию для добавления в начало нескольких элементов

//--------------------------------------- Удаление в массиве

// pop() - удаление с конца массива 
// shift() - удаление с начала массива 


// let myArr = [2, 5, 7, 11, 25];
// console.log(myArr);

// myArr.pop();
// console.log(myArr);

// myArr.shift()
// console.log(myArr);

// let myArray = [2, 3, 5, 8, 13, 21, 34];
// console.log(myArray);

// function myDel() {
//     for (let i = myArray.length - 1; i >= 0; i--) {
//         if (myArray[i] !== 3) {
//             myArray.pop();
//         } else {
//             return;
//         }
//     }   
// }
// myDel();
// console.log(myArray);

//-------------------------------------------- Удаление в середине массива

// slice() - удаляет с указанием какого индекса укзано. Создает копию. 
// Первый параметр = стартовый индекс. Второй параметр = индекс, который в новый массив не войдет. Всегда возвращает массив!!!!!!!!!!!!

// splice() - портит весь массив. Не делает копии массива. Позволяет удалять из середины массива. При удалении возвращает на консоль элемент
// Удаляет старый элемент и добавляет новый. 

// let myArray = [2, 3, 5, 8, 13, 21, 34];
// console.log(myArray);
// console.log(myArray.slice(2));
// console.log(myArray.slice(2, 4));
// console.log(myArray.slice(myArray.length-1));
// console.log(myArray.slice(-1));
// // console.log(myArray);

// console.log(myArray.splice(2, 1));
// console.log(myArray);
// console.log(myArray.splice(2, 1, 250));
// console.log(myArray);

// console.log("Индекс:", myArray.indexOf(5));

//----------------------------------------------------------------------

// indexOf() - внутрь скобок указываем элемент, показывает индекс

// function deleteNumber(number) {
//    const index = myArray.indexOf(number);
//    if (index >= 0) {
//     myArray.splice(index, 1);
//    }   
// }

// deleteNumber(13);

// console.log(myArray);

//------------------------------------------------------------------------

// filter() - перебирает все элементы. Первый параметр - элемент итерации, вторым - индекс, третьим - массив(часто не используется).
// Обязательно return и возвращает boolean значение

// let myArr = [2, 3, 5, 8, 13, 21, 34];

// function forFilter(item) {
//     return (item < 6 || item > 19);    
// }

// myArr = myArr.filter(forFilter);
// console.log(myArr);

// массив. Из массива выбрать элементы, которые меньше 10 и получить их сумму

//-----------------------------------------

// find()- перебирает массив до указанного элемента или индекса. Первый параметр - элемент итерации, вторым - индекс, третьим- массив. 
// Если ничего не нашло, тогда underfind

// let myArr = [2, 3, 5, 8, 13, 21, 34];

// function findNumber(item) {
//     return item > 6;
// }
// const item = myArr.find(findNumber);
// if (item) {
//     console.log("Число:", item);
//     let index = myArr.indexOf(item);
//     myArr[index] = 0;
//     // console.log("Индекс:", index);
//     // myArr.splice(index, 1, 200);
// }

// console.log(myArr);

//-----------------------------------------------------------

// map() - Ждет на вход функцию. Элемент итерации и индекс. 
// Перебирает все элементы. На выходе дает новый массив.  

// let myArr = [2, 3, 5, 8, 13, 21, 34];
// console.log("Исходный массив:", myArr);

// function addOne(item) {
//     return item + 1;
// }
// myArr = myArr.map(addOne);
// console.log("Полученный массив:", myArr);

// function isMore(item) {
//     if (item >= 12) {
//         return 'больше';
//     } else {
//         return 'меньше';
//     }
// }

// myArr = myArr.map(isMore);
// console.log(myArr);

//----------------------------- Преобразование строки к числу

//  let myArr = ['2', '3', '5', 'rgrg', '13', '21', '34'];
//  console.log("Исходный массив:", myArr);

//  function getNumber(item) {
//      if (isNaN(+item)) {
//          return 0;
//      } else {
//         return +item;
//      }    
//  }

//  function getNum(item) {
//      if (isNaN(+item)) {
//          return 0;
//      } else {
//        return +item <= 4 ? 0 : 1;
//      }
//  }
//  myArr = myArr.map(getNumber); 
// //  console.log("Полученный массив:", myArr);
//  myArr = myArr.map(getNum);
//  console.log("Полученный массив:", myArr);

 // Из существующего массива чисел, получить массив с названиеями чисел от 0 до 9 

 //---------------------------------------------------------- Switch()
// Правилами хорошего тона писать default. Не забывать про break.


//------------------------------------------------------------- Reduce()
// reduce()- Ждет функцию. На первом месте переменная аккумулятор. Вторым- элемент итерации, третьим- индекс, четвертым - весь массив

 let myArr = [2, 3, 5, 8, 13, 21, 34];
 console.log("Исходный массив:", myArr);

 function getSumm(accum, item) {
     return accum + item;
 }

 console.log("Сумма элементов массива:", myArr.reduce(getSumm, 0));
